import sys
import collections
import gdspy


def merge_layers_and_datatypes(cell):
    print(f"Processing cell {cell.name}")

    polygonSet = collections.defaultdict(list)
    for p in cell.polygons:
        for l, d, points in zip(p.layers, p.datatypes, p.polygons):
            polygonSet[(l, d)].append(points)

    cell.polygons = []

    for (l, d), plist in polygonSet.items():
        if len(plist) > 1:
            print(f"  Layer {l}, datatype {d}: {len(plist)} polygons to merge")
            result = gdspy.boolean(plist, None, "or", layer=l, datatype=d)
        elif len(plist) == 1:
            cell.add(gdspy.Polygon(plist[0], layer=l, datatype=d))


if len(sys.argv) < 2:
    print(f"Missing filename\nUsage: python3 {sys.argv[0]} FILENAME")
    exit(1)

filename = sys.argv[1]
print(f"Merging all shapes in {filename}")

lib = gdspy.GdsLibrary(infile=filename)
for cell in lib.cells.values():
    merge_layers_and_datatypes(cell)
filename = filename.split('/')[-1].split('.')[0]
print('filename',filename)
lib.write_gds(f"{filename}_merged_polygons.gds")