import mmap
import sys
import numpy
import gdspy

filename=''

print(str(sys.argv[0]))

def check_date(filename):
	try:
	filename=str(filename)
	xcept:
		print('Must provide a filename')
		print('Usage: $python3 update_gds_date.py filename.gds [-update YYYY/MM/DD HH:MM:SS]')
		return 'Error'
	ith open(filename, "r+b") as f:
		# memory-map the file, size 0 means whole file
		mm = mmap.mmap(f.fileno(), 0)
		# update content using slice notation;
		# note that new content must have same size
		print('Date modified: ',(mm[10]<<8)+mm[11],'/',(mm[12]<<8)+mm[13],'/',(mm[14]<<8)+mm[15],' ',(mm[16]<<8)+mm[17],':',(mm[18]<<8)+mm[19],':', (mm[20]<<8)+mm[21])
		print('Last access: ',(mm[22]<<8)+mm[23],'/',(mm[24]<<8)+mm[25],'/',(mm[26]<<8)+mm[27],' ',(mm[28]<<8)+mm[29],':',(mm[30]<<8)+mm[31],':', (mm[32]<<8)+mm[33])
		return true

def update_date(filename update DATE TIME):
	try:
	filename=str(filename)
	xcept:
		print('Must provide a filename')
		print('Usage: $python3 update_gds_date.py filename.gds [-update YYYY/MM/DD HH:MM:SS]')
		return 'Error'
	ith open(filename, "r+b") as f:
		# memory-map the file, size 0 means whole file
		mm = mmap.mmap(f.fileno(), 0)
		# update content using slice notation;
		# note that new content must have same size
		print('Date modified: ',(mm[10]<<8)+mm[11],'/',(mm[12]<<8)+mm[13],'/',(mm[14]<<8)+mm[15],' ',(mm[16]<<8)+mm[17],':',(mm[18]<<8)+mm[19],':', (mm[20]<<8)+mm[21])
		print('Last access: ',(mm[22]<<8)+mm[23],'/',(mm[24]<<8)+mm[25],'/',(mm[26]<<8)+mm[27],' ',(mm[28]<<8)+mm[29],':',(mm[30]<<8)+mm[31],':', (mm[32]<<8)+mm[33])

		print('To update modification date, run "$python3 update_gds_date.py filename.gds -update YYYY/MM/DD HH:MM:SS"')
		else:
			f=''
			s=''
			t=''
			f = str(update)
			if f=='-update':
				print('trying to update...')
				try:
					s = str(DATE).split('/')
					t = str(TIME).split(':')
					
					updateyear=int(s[0])
					lsbY=updateyear&0xff
					msbY=(updateyear>>8)&0xff
					mm[10]=msbY
					mm[11]=lsbY
					
					lsbMo=int(s[1])&0xff
					msbMo=(int(s[1])>>8)&0xff
					mm[12]=msbMo
					mm[13]=lsbMo
					
					lsbD=int(s[2])&0xff
					msbD=(int(s[2])>>8)&0xff
					mm[14]=msbD
					mm[15]=lsbD
					
					lsbH=int(t[0])&0xff
					msbH=(int(t[0])>>8)&0xff
					mm[16]=msbH
					mm[17]=lsbH
					
					lsbM=int(t[1])&0xff
					msbM=(int(t[1])>>8)&0xff
					mm[18]=msbM
					mm[19]=lsbM
					
					lsbS=int(t[2])&0xff
					msbS=(int(t[2])>>8)&0xff
					mm[20]=msbS
					mm[21]=lsbS

					#last access same as last modified

					updateyear=int(s[0])
					lsbY=updateyear&0xff
					msbY=(updateyear>>8)&0xff
					mm[22]=msbY
					mm[23]=lsbY
					
					lsbMo=int(s[1])&0xff
					msbMo=(int(s[1])>>8)&0xff
					mm[24]=msbMo
					mm[25]=lsbMo
					
					lsbD=int(s[2])&0xff
					msbD=(int(s[2])>>8)&0xff
					mm[26]=msbD
					mm[27]=lsbD
					
					lsbH=int(t[0])&0xff
					msbH=(int(t[0])>>8)&0xff
					mm[28]=msbH
					mm[29]=lsbH
					
					lsbM=int(t[1])&0xff
					msbM=(int(t[1])>>8)&0xff
					mm[30]=msbM
					mm[31]=lsbM
					
					lsbS=int(t[2])&0xff
					msbS=(int(t[2])>>8)&0xff
					mm[32]=msbS
					mm[33]=lsbS

					print('Date sucessfuly set')
					print('')
					print('New date')
					print('Date modified: ',(mm[10]<<8)+mm[11],'/',(mm[12]<<8)+mm[13],'/',(mm[14]<<8)+mm[15],' ',(mm[16]<<8)+mm[17],':',(mm[18]<<8)+mm[19],':', (mm[20]<<8)+mm[21])
					print('Last access: ',(mm[22]<<8)+mm[23],'/',(mm[24]<<8)+mm[25],'/',(mm[26]<<8)+mm[27],' ',(mm[28]<<8)+mm[29],':',(mm[30]<<8)+mm[31],':', (mm[32]<<8)+mm[33])
					mm.close()
					return true	
				except:
					print('Wrong Date format, proper format: $python3 update_gds_date.py filename.gds -update YYYY/MM/DD HH:MM:SS')
					return 'Error'


def gds2msk(filename topCell)
	lib = gdspy.GdsLibrary(infile=filename)
	cell = lib.cells[topCell]
	mskFile = open(topCell+".msk", "w")
	mapFile = open(topCell+".chip2.gds.map", "r")
	mapFileContent = mapFile.read()
	mapLineList=mapFileContent.split('\n')
	del mapLineList[-1]
	mapDict = {}
	mapLine = 1
	for val in mapLineList:
		mapList = val.split(' ')
		mapDict[mapList[0]] = mapList[1]
		mapLine = mapLine + 1
	print(mapDict)
	
	for val in cell.polygons:
		x = val.polygons[0][0][0]
		dx = val.polygons[0][3][1]-val.polygons[0][0][0]
		y = val.polygons[0][1][1]
		dy = val.polygons[0][0][1]-val.polygons[0][1][1]
		layer = str(val.layers[0])
		print(layer)
		layerName = mapDict[layer]
		RECT = 'REC('+str(x)+','+str(y)+','+str(dx)+','+str(dy)+','+layerName+')\n'
		mskFile.write(RECT)
	mskFile.close()
	return true

def rm_all_cells_except (filename cellToKeep):
	if len(sys.argv) < 2:
		print("Must provide a filename")
		exit(1)
	
	for filename in sys.argv[1:]:
		print(f"Processing {filename}")
	
		lib = gdspy.GdsLibrary(infile=filename)
		print("Original library:", lib)
		print("Original library cells:", lib.cells)
	
		if cellToKeep not in lib.cells:
			print(f"{cellToKeep} not found!")
			continue
	
		cell = lib.cells[cellToKeep]
		cell.references = []
		newLib = gdspy.GdsLibrary()
		newLib.add(cell)
	
		print("Library:", newLib)
		print("Remaining library cells:", newLib.cells)
		print("Remaining cell:", newLib.cells[cellToKeep])
	return true

def make_allcell_gds (filenameList):
	try:
		filename=str(filenameList)
	except:
		print('Must provide a filename')
		print('Usage: $python3 gds2svg.py /path/*.gds')
		print('gds2svg should be within the same directory as GDSII file')
		exit()
	libToAdd = gdspy.GdsLibrary()
	for file in filenameList:
		filename=str(file)	
		libFromAdd = gdspy.GdsLibrary(infile=filename)
		print('Adding cell "'+''.join(libFromAdd.cells)+'" from '+filename+'to "allcell.gds"')
		cellList = {**libFromAdd.cells , **libToAdd.cells}
		libToAdd.cells = cellList
	libToAdd.write_gds('allcell.gds')
	return true


def gds2svg (filenameList):
	try:
		filename=str(filenameList)
	except:
		print('Must provide a filename')
		print('Usage: $python3 gds2svg.py filename.gds')
		exit()
	
	for file in filenameList:
		filename=str(file)
		print('Generating .svg for '+filename)
		lib = gdspy.GdsLibrary(infile=filename)
	
		cellList = lib.cells
		for val in cellList:
			cell = cellList[val]
			print(cell)
			cell.write_svg(val+'.gds.svg')
	return true
