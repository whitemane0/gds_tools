import sys
import gdspy

if len(sys.argv) < 2:
    print(f"Missing filename\nUsage: python3 {sys.argv[0]} FILENAME")
    exit(1)

filename = sys.argv[1]
lib = gdspy.GdsLibrary(infile=filename)

gdspy.LayoutViewer(library=lib, cells=None, hidden_types=[], depth=50, color={}, pattern={}, background='#303030', width=1280, height=1024)

exit()
