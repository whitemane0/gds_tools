**GDS tools to work with GDSII binary files**

update_gds_date.py

Show date modified and last access of GDSII file provided.
Allows to change date modified and last-accessed.

## Usage

Usage: $python3 update_gds_date.py filename.gds [-update YYYY/MM/DD HH:MM:SS]

## Credits
Thanks to @lynch (https://bitbucket.org/lynch) for guidence.

Link to GDSII binary format desctription:
https://www.iue.tuwien.ac.at/phd/minixhofer/node52.html