import numpy
import gdspy
lib = gdspy.GdsLibrary(infile='chip2.gds')
cell = lib.cells["chip2"]
mskFile = open("chip2.msk", "w")
mapFile = open("chip2.gds.map", "r")
mapFileContent = mapFile.read()
mapLineList=mapFileContent.split('\n')
del mapLineList[-1]
mapDict = {}
mapLine = 1
for val in mapLineList:
	mapList = val.split(' ')
	mapDict[mapList[0]] = mapList[1]
	mapLine = mapLine + 1
print(mapDict)

for val in cell.polygons:
	#REC(184,-18,1328,47,NW)
	x = val.polygons[0][0][0]
	dx = val.polygons[0][3][1]-val.polygons[0][0][0]
	y = val.polygons[0][1][1]
	dy = val.polygons[0][0][1]-val.polygons[0][1][1]
	layer = str(val.layers[0])
	print(layer)
	layerName = mapDict[layer]
	RECT = 'REC('+str(x)+','+str(y)+','+str(dx)+','+str(dy)+','+layerName+')\n'
	mskFile.write(RECT)
mskFile.close()
