import numpy
import gdspy
import sys

filename=''

del sys.argv[0]

try:
	filename=str(sys.argv[0])
except:
	print(f'Must provide a filename')
	print(f'do_allcell create a gds file containing all cells from given gds list')
	print(f'Usage example: do_allcell /path/*.gds')
	exit()

libToAdd = gdspy.GdsLibrary()

for file in sys.argv:
	filename=str(file)
	
	libFromAdd = gdspy.GdsLibrary(infile=filename)
	print('Adding cell "'+''.join(libFromAdd.cells)+'" from '+filename+'to "allcell.gds"')
	cellList = {**libFromAdd.cells , **libToAdd.cells}
	libToAdd.cells = cellList
	
libToAdd.write_gds('allcell.gds')
