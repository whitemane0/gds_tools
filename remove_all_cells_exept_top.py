import gdspy
import sys

if len(sys.argv) < 2:
    print("Must provide a filename")
    exit(1)

cellToKeep = "ASIC_pad_ring"

for filename in sys.argv[1:]:
    print(f"Processing {filename}")

    lib = gdspy.GdsLibrary(infile=filename)
    print("Original library:", lib)
    print("Original library cells:", lib.cells)

    if cellToKeep not in lib.cells:
        print(f"{cellToKeep} not found!")
        continue

    cell = lib.cells[cellToKeep]
    cell.references = []
    newLib = gdspy.GdsLibrary()
    newLib.add(cell)

    print("Library:", newLib)
    print("Remaining library cells:", newLib.cells)
    print("Remaining cell:", newLib.cells[cellToKeep])