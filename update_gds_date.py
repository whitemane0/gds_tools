import mmap
import sys

filename=''

print(str(sys.argv[0]))

try:
   filename=str(sys.argv[1])
except:
   print('Must provide a filename')
   print('Usage: $python3 update_gds_date.py filename.gds [-update YYYY/MM/DD HH:MM:SS]')
   exit()
with open(filename, "r+b") as f:
    # memory-map the file, size 0 means whole file
    mm = mmap.mmap(f.fileno(), 0)
    # update content using slice notation;
    # note that new content must have same size
    print('Date modified: ',(mm[10]<<8)+mm[11],'/',(mm[12]<<8)+mm[13],'/',(mm[14]<<8)+mm[15],' ',(mm[16]<<8)+mm[17],':',(mm[18]<<8)+mm[19],':', (mm[20]<<8)+mm[21])
    print('Last access: ',(mm[22]<<8)+mm[23],'/',(mm[24]<<8)+mm[25],'/',(mm[26]<<8)+mm[27],' ',(mm[28]<<8)+mm[29],':',(mm[30]<<8)+mm[31],':', (mm[32]<<8)+mm[33])
       
    #print(len(sys.argv))

    if len(sys.argv)<3:
        print('To update modification date, run "$python3 update_gds_date.py filename.gds -update YYYY/MM/DD HH:MM:SS"')
    else:

        f=''
        s=''
        t=''
        f = str(sys.argv[2])
        if f=='-update':
        
            print('trying to update...')
            try:
                s = str(sys.argv[3]).split('/')
                t = str(sys.argv[4]).split(':')

                updateyear=int(s[0])
                lsbY=updateyear&0xff
                msbY=(updateyear>>8)&0xff
                mm[10]=msbY
                mm[11]=lsbY
        
                lsbMo=int(s[1])&0xff
                msbMo=(int(s[1])>>8)&0xff
                mm[12]=msbMo
                mm[13]=lsbMo
        
                lsbD=int(s[2])&0xff
                msbD=(int(s[2])>>8)&0xff
                mm[14]=msbD
                mm[15]=lsbD
    
                lsbH=int(t[0])&0xff
                msbH=(int(t[0])>>8)&0xff
                mm[16]=msbH
                mm[17]=lsbH
    
                lsbM=int(t[1])&0xff
                msbM=(int(t[1])>>8)&0xff
                mm[18]=msbM
                mm[19]=lsbM
    
                lsbS=int(t[2])&0xff
                msbS=(int(t[2])>>8)&0xff
                mm[20]=msbS
                mm[21]=lsbS
    
    #last access same as last modified
    
                updateyear=int(s[0])
                lsbY=updateyear&0xff
                msbY=(updateyear>>8)&0xff
                mm[22]=msbY
                mm[23]=lsbY
        
                lsbMo=int(s[1])&0xff
                msbMo=(int(s[1])>>8)&0xff
                mm[24]=msbMo
                mm[25]=lsbMo
        
                lsbD=int(s[2])&0xff
                msbD=(int(s[2])>>8)&0xff
                mm[26]=msbD
                mm[27]=lsbD
    
                lsbH=int(t[0])&0xff
                msbH=(int(t[0])>>8)&0xff
                mm[28]=msbH
                mm[29]=lsbH
    
                lsbM=int(t[1])&0xff
                msbM=(int(t[1])>>8)&0xff
                mm[30]=msbM
                mm[31]=lsbM
    
                lsbS=int(t[2])&0xff
                msbS=(int(t[2])>>8)&0xff
                mm[32]=msbS
                mm[33]=lsbS

                print('Date sucessfuly set')
                print('')
                print('New date')
                print('Date modified: ',(mm[10]<<8)+mm[11],'/',(mm[12]<<8)+mm[13],'/',(mm[14]<<8)+mm[15],' ',(mm[16]<<8)+mm[17],':',(mm[18]<<8)+mm[19],':', (mm[20]<<8)+mm[21])
                print('Last access: ',(mm[22]<<8)+mm[23],'/',(mm[24]<<8)+mm[25],'/',(mm[26]<<8)+mm[27],' ',(mm[28]<<8)+mm[29],':',(mm[30]<<8)+mm[31],':', (mm[32]<<8)+mm[33])

                mm.close()

            except:
                print('Wrong Date format, proper format: $python3 update_gds_date.py filename.gds -update YYYY/MM/DD HH:MM:SS')
                exit()
               