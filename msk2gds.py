import numpy
import gdspy
import re

lib = gdspy.GdsLibrary()
cell = lib.new_cell('TOP')

mskFile = open("semiseg.msk", "r")
mskFileContent = mskFile.read()
rectExp = re.compile('REC\((.*)\)')
rectList = re.findall(rectExp, mskFileContent)
rectList.sort(key=lambda l: l.split(',')[4],reverse=False)

rectLayerName = ''
gdsNum = 1
gdsRectList = []
currentRect = ''
for val in rectList:
	currentRectLayerName = re.match('.*\,(.*)',val).group(1)
	if rectLayerName == currentRectLayerName:
		currentRect = re.sub(currentRectLayerName,str(gdsNum),val)
		currentRectExplode = currentRect.split(',',5)
		currentRectExplode[0]=int(currentRectExplode[0])
		currentRectExplode[1]=int(currentRectExplode[1])
		currentRectExplode[2]=int(currentRectExplode[2])
		currentRectExplode[3]=int(currentRectExplode[3])
		rect = gdspy.Rectangle((currentRectExplode[0],currentRectExplode[1]),(currentRectExplode[0]+currentRectExplode[2],currentRectExplode[1]+currentRectExplode[3]),gdsNum,0)
		cell.add(rect)
	else:
		rectLayerName = currentRectLayerName
		gdsNum=gdsNum + 1
		print(rectLayerName,gdsNum)
		currentRect = re.sub(currentRectLayerName,str(gdsNum),val)
		currentRectExplode = currentRect.split(',',5)
		currentRectExplode[0]=int(currentRectExplode[0])
		currentRectExplode[1]=int(currentRectExplode[1])
		currentRectExplode[2]=int(currentRectExplode[2])
		currentRectExplode[3]=int(currentRectExplode[3])
		rect = gdspy.Rectangle((currentRectExplode[0],currentRectExplode[1]),(currentRectExplode[0]+currentRectExplode[2],currentRectExplode[1]+currentRectExplode[3]),gdsNum,0)
		cell.add(rect)

cell.write_svg('semiseg.svg')

lib.write_gds('semiseg.gds')		
