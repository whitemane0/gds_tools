import numpy
import gdspy
import sys

filename=''

del sys.argv[0]

try:
	filename=str(sys.argv[0])
except:
	print(f"Must provide a filename")
	print(f"Usage example: gds2svg ../path_to/gds_files/*/*latest.gds")
	exit()

for file in sys.argv:
	filename=str(file)
	print(f"Generating .svg files for {filename}")
	lib = gdspy.GdsLibrary(infile=filename)

	cellList = lib.cells
	for val in cellList:
		cell = cellList[val]
		print(f"Processing {cell}")
		cell.write_svg(f"{val}.gds.svg")
