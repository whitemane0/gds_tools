import numpy
import gdspy
import sys
import copy

#	input("Press Enter to continue...")


filename=''

try:
	filename=str(sys.argv[1])
except:
	print('Must provide a filename')
	exit()

cellsToRemove = ''

print('Processing '+filename)
lib = gdspy.GdsLibrary(infile=filename)
topCell = lib.top_level()[0]
print('TopCellList: ',topCell,'\n\n')

cellsToLeave = []
for c in topCell.references:
#	print(dir(c))
	print(str(c))
#	if c :
	if 'FILL' in str(c)  or 'VIA' in str(c):
		print('Remove ',c)
		c = []
print(topCell)
#topCell.references = []
topCell = topCell.flatten()
print('TopCell: ',topCell,'\n\n')
lib.cells[topCell.name].write_svg('ASIC_pad_ring.gds.svg')
lib.write_gds(topCell.name+'_only.gds')
